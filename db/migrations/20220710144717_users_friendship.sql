-- +goose Up
-- +goose StatementBegin

CREATE TABLE users_friendship
(
    id        serial not null unique,
    user_id   int not null references users (id) on delete cascade,
    friend_id int not null references users (id) on delete cascade,
    PRIMARY KEY (id),
    UNIQUE KEY user_friend_unique (user_id, friend_id)
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin

DROP TABLE users_friendship

-- +goose StatementEnd
