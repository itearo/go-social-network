-- +goose Up
-- +goose StatementBegin

CREATE TABLE chat_message_unread
(
    id        serial not null unique,
    sender_id   int not null references users (id) on delete cascade,
    addressee_id int not null references users (id) on delete cascade,
    counter int not null default 0,
    PRIMARY KEY (id),
    UNIQUE KEY sender_addressee_unique (sender_id, addressee_id)
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin

DROP TABLE chat_message_unread;

-- +goose StatementEnd
