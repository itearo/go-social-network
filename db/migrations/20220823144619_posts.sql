-- +goose Up
-- +goose StatementBegin

CREATE TABLE posts
(
    id        serial not null unique,
    message   text   not null,
    author_id int    not null
) COLLATE utf8mb4_general_ci;

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin

DROP TABLE posts;

-- +goose StatementEnd
