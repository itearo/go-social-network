-- +goose Up
-- +goose StatementBegin

CREATE TABLE users
(
    id            serial       not null unique,
    email         varchar(255) not null unique,
    password_hash varchar(255) not null,
    name          varchar(255) not null,
    family_name   varchar(255) not null,
    age           varchar(255) not null,
    sex           varchar(255) not null,
    interests     varchar(255) not null,
    city          varchar(255) not null
) COLLATE utf8mb4_general_ci;

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin

DROP TABLE users;

-- +goose StatementEnd
