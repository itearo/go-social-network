package environment

import (
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
)

var Config EnvConfig

type EnvConfig struct {
	AmqpDsn                       string
	AmqpQueuePostsFeed            string
	AmqpExchangePostsFeed         string
	AmqpQueueInboundNotification  string
	AmqpQueueOutboundNotification string
	RedisHost                     string
	RedisPort                     string
	RedisPass                     string
	RedisDbName                   int
	DbName                        string
	MasterHost                    string
	MasterPort                    string
	MasterUser                    string
	MasterPass                    string
	HaproxyHost                   string
	HaproxyPort                   string
	TarantoolHost                 string
	TarantoolPort                 string
	FeedCacheThreshold            int
	ShardKey                      string
	JwtSigningKey                 string
	ChatApiUrl                    string
}

func Boot() {
	if err := godotenv.Overload(".env", ".env.local"); err != nil {
		logrus.Warn("Environment file not loaded. Error: ", err.Error())
	}

	redisDbName, _ := strconv.Atoi(os.Getenv("REDIS_DB"))

	feedCacheThreshold, _ := strconv.Atoi(os.Getenv("FEED_CACHE_THRESHOLD"))

	Config = EnvConfig{
		AmqpDsn:                       os.Getenv("AMQP_DSN"),
		AmqpQueuePostsFeed:            os.Getenv("AMQP_QUEUE_POSTS_FEED"),
		AmqpExchangePostsFeed:         os.Getenv("AMQP_EXCHANGE_POSTS_FEED"),
		AmqpQueueInboundNotification:  os.Getenv("AMQP_QUEUE_INBOUND_NOTIFICATION"),
		AmqpQueueOutboundNotification: os.Getenv("AMQP_QUEUE_OUTBOUND_NOTIFICATION"),
		RedisHost:                     os.Getenv("REDIS_HOST"),
		RedisPort:                     os.Getenv("REDIS_PORT"),
		RedisPass:                     os.Getenv("REDIS_PASS"),
		RedisDbName:                   redisDbName,
		DbName:                        os.Getenv("DB_NAME"),
		MasterHost:                    os.Getenv("MASTER_HOST"),
		MasterPort:                    os.Getenv("MASTER_PORT"),
		MasterUser:                    os.Getenv("MASTER_USER"),
		MasterPass:                    os.Getenv("MASTER_PASS"),
		HaproxyHost:                   os.Getenv("HAPROXY_HOST"),
		HaproxyPort:                   os.Getenv("HAPROXY_PORT"),
		TarantoolHost:                 os.Getenv("TARANTOOL_HOST"),
		TarantoolPort:                 os.Getenv("TARANTOOL_PORT"),
		FeedCacheThreshold:            feedCacheThreshold,
		ShardKey:                      os.Getenv("SHARD_KEY"),
		JwtSigningKey:                 os.Getenv("JWT_SIGNING_KEY"),
		ChatApiUrl:                    os.Getenv("CHAT_API_URL"),
	}
}
