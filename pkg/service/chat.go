package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"social-network-backend/pkg/model"
	"social-network-backend/pkg/repository/mysql"
	"time"
)

type ChatService struct {
	chatApiUrl     string
	amqpService    *AmqpService
	chatRepository *mysql.ChatMysqlRepository
}

func NewChatService(chatApiUrl string, amqpService *AmqpService, chatRepository *mysql.ChatMysqlRepository) *ChatService {
	return &ChatService{chatApiUrl: chatApiUrl, amqpService: amqpService, chatRepository: chatRepository}
}

func (s *ChatService) SaveNewMessage(authToken string, friendId int, message string, correlationId string) error {
	data := bytes.NewReader([]byte(fmt.Sprintf(`{"message": "%s"}`, message)))
	request, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/chat/%d", s.chatApiUrl, friendId), data)
	if err != nil {
		return err
	}

	responseBody, err := executeRequest(request, authToken, correlationId)
	if err != nil {
		return err
	}

	logrus.Info(string(responseBody))

	return nil
}

func (s *ChatService) GetChatMessages(authToken string, friendId int, correlationId string) ([]model.ChatMessage, error) {
	request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/chat/%d", s.chatApiUrl, friendId), nil)
	if err != nil {
		return nil, err
	}

	responseBody, err := executeRequest(request, authToken, correlationId)
	if err != nil {
		return nil, err
	}

	logrus.Info(string(responseBody))

	var chats struct {
		Data []model.ChatMessage `json:"data"`
	}

	err = json.Unmarshal(responseBody, &chats)
	if err != nil {
		return nil, err
	}

	return chats.Data, nil
}

func (s *ChatService) GetChats(authToken string, correlationId string) ([]model.Chat, error) {
	request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/chat", s.chatApiUrl), nil)
	if err != nil {
		return nil, err
	}

	responseBody, err := executeRequest(request, authToken, correlationId)
	if err != nil {
		return nil, err
	}

	logrus.Info(string(responseBody))

	var chats struct {
		Data []model.Chat `json:"data"`
	}

	err = json.Unmarshal(responseBody, &chats)
	if err != nil {
		return nil, err
	}

	return chats.Data, nil
}

func executeRequest(req *http.Request, authToken string, correlationId string) ([]byte, error) {
	req.Header.Set("Authorization", authToken)
	req.Header.Set("X-Correlation-ID", correlationId)

	client := http.Client{Timeout: 30 * time.Second}

	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return responseBody, nil
}

func (s *ChatService) GetUnreadMessagesCount(addresseeId int) (int, error) {
	return s.chatRepository.GetUnreadMessagesCount(addresseeId)
}

func (s *ChatService) ListenForInboundNotificationEvents() error {
	events, err := s.amqpService.ConsumeInboundNotificationEvents()
	if err != nil {
		return err
	}

	var chatEvent ChatEvent

	for event := range events {
		logrus.Infof("received a message: %s", event.Body)

		err = json.Unmarshal(event.Body, &chatEvent)
		if err != nil {
			logrus.Error("could not deserialize event: %s", err)
		}

		if chatEvent.Type == "messages-read" {
			var chatMessagesReadEvent ChatMessagesReadEvent

			err = json.Unmarshal(chatEvent.Data, &chatMessagesReadEvent)
			if err != nil {
				logrus.Error("could not deserialize event: %s", err)
			}

			logrus.Info("clear unread messages count for chat")

			err = s.chatRepository.ClearUnreadMessages(chatMessagesReadEvent.SenderId, chatMessagesReadEvent.AddresseeId)
			if err != nil {
				logrus.Error("could not clear unread messages: %s", err)
			}
		} else if chatEvent.Type == "message-added" {
			var chatMessageAddedEvent ChatMessageAddedEvent

			err = json.Unmarshal(chatEvent.Data, &chatMessageAddedEvent)
			if err != nil {
				logrus.Error("could not deserialize event: %s", err)
			}

			logrus.Info("add 1 to unread message count")
			err = s.chatRepository.AddOneUnreadMessage(chatMessageAddedEvent.SenderId, chatMessageAddedEvent.AddresseeId)
			if err != nil {
				logrus.Errorf("could not add 1 to unread message count: %s", err)

				_ = s.amqpService.PublishUnreadMessageCounterErrorEvent(chatMessageAddedEvent.CorrelationId)
			}
		} else {
			logrus.Errorf("unknown event: %s", chatEvent)
		}
	}

	return nil
}

type ChatMessageAddedEvent struct {
	SenderId      int    `json:"senderId"`
	AddresseeId   int    `json:"addresseeId"`
	CorrelationId string `json:"correlationId"`
}

type ChatMessagesReadEvent struct {
	SenderId    int `json:"senderId"`
	AddresseeId int `json:"addresseeId"`
}

type ChatEvent struct {
	Type string
	Data json.RawMessage
}
