package service

import (
	"social-network-backend/pkg/environment"
	"social-network-backend/pkg/repository"
)

type Service struct {
	*AuthService
	*UserService
	*PostService
	*AmqpService
	*RedisService
	*CacheService
	*ChatService
}

func NewServices(repos *repository.Repository, config environment.EnvConfig) *Service {
	userService := NewUserService(repos.UserRepository)
	postService := NewPostService(repos.PostRepository, repos.PostTarantoolRepository)
	amqpService := NewAmqpService()
	redisService := NewRedisService()

	return &Service{
		AuthService:  NewAuthService(config.JwtSigningKey, repos.AuthRepository),
		UserService:  userService,
		PostService:  postService,
		AmqpService:  amqpService,
		RedisService: redisService,
		CacheService: NewCacheService(userService, postService, amqpService, redisService),
		ChatService:  NewChatService(config.ChatApiUrl, amqpService, repos.ChatRepository),
	}
}
