package service

import (
	"social-network-backend/pkg/model"
	"social-network-backend/pkg/repository/mysql"
)

type UserService struct {
	repo *mysql.UserMysql
}

func NewUserService(repo *mysql.UserMysql) *UserService {
	return &UserService{repo: repo}
}

func (s *UserService) GetAll() ([]model.UserList, error) {
	return s.repo.GetAll()
}

func (s *UserService) AddFriend(userId, friendId int) error {
	return s.repo.AddFriend(userId, friendId)
}

func (s *UserService) GetById(id int) (model.UserList, error) {
	return s.repo.GetById(id)
}

func (s *UserService) GetByQuery(query string) ([]model.UserList, error) {
	return s.repo.GetByQuery(query)
}
