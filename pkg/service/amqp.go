package service

import (
	"context"
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
	"os"
	"social-network-backend/pkg/environment"
	"time"
)

type AmqpService struct {
}

func NewAmqpService() *AmqpService {
	return &AmqpService{}
}

func (s *AmqpService) ConsumeInboundNotificationEvents() (<-chan amqp.Delivery, error) {
	amqpChannel, err := createAmqpChannel(environment.Config.AmqpQueueInboundNotification)
	if err != nil {
		return nil, err
	}

	events, err := amqpChannel.Consume(
		environment.Config.AmqpQueueInboundNotification,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	return events, err
}

func (s *AmqpService) PublishUnreadMessageCounterErrorEvent(correlationId string) error {
	amqpChannel, err := createAmqpChannel(environment.Config.AmqpQueueOutboundNotification)
	if err != nil {
		return err
	}

	err = amqpChannel.ExchangeDeclare(
		environment.Config.AmqpQueueOutboundNotification,
		"direct",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	err = amqpChannel.QueueBind(
		environment.Config.AmqpQueueOutboundNotification,
		environment.Config.AmqpQueueOutboundNotification,
		environment.Config.AmqpQueueOutboundNotification,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	type UnreadMessageCounterError struct {
		CorrelationId string `json:"correlationId"`
	}

	event := UnreadMessageCounterError{
		CorrelationId: correlationId,
	}

	serializedEvent, err := json.Marshal(event)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = amqpChannel.PublishWithContext(
		ctx,
		environment.Config.AmqpQueueOutboundNotification,
		environment.Config.AmqpQueueOutboundNotification,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        serializedEvent,
		},
	)

	return err
}

func (s *AmqpService) ConsumeFeedCacheEvents() (<-chan amqp.Delivery, error) {
	amqpChannel, err := createAmqpChannel(environment.Config.AmqpQueuePostsFeed)
	if err != nil {
		return nil, err
	}

	events, err := amqpChannel.Consume(
		environment.Config.AmqpQueuePostsFeed,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	return events, err
}

func (s *AmqpService) PublishFeedCacheEvent(event RebuildFeedCacheEvent) error {
	amqpChannel, err := createAmqpChannel(environment.Config.AmqpExchangePostsFeed)
	if err != nil {
		return err
	}

	err = amqpChannel.ExchangeDeclare(
		environment.Config.AmqpExchangePostsFeed,
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	err = amqpChannel.QueueBind(
		environment.Config.AmqpQueuePostsFeed,
		"feed.cache.*",
		environment.Config.AmqpExchangePostsFeed,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	serializedEvent, err := json.Marshal(event)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = amqpChannel.PublishWithContext(
		ctx,
		environment.Config.AmqpExchangePostsFeed,
		makeRoutingKey(event.UserId),
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        serializedEvent,
		},
	)

	return err
}

func (s *AmqpService) ConsumeFeedReloadEvents() (<-chan amqp.Delivery, error) {
	amqpChannel, err := createAmqpChannel(makeShardQueueName())
	if err != nil {
		return nil, err
	}

	events, err := amqpChannel.Consume(
		makeShardQueueName(),
		"",
		true,
		true,
		false,
		false,
		nil,
	)

	return events, err
}

func (s *AmqpService) BindUser(userId int) error {
	amqpChannel, err := createAmqpChannel(makeShardQueueName())
	if err != nil {
		return err
	}

	return amqpChannel.QueueBind(
		makeShardQueueName(),
		makeRoutingKey(userId),
		environment.Config.AmqpExchangePostsFeed,
		false,
		nil,
	)
}

func (s *AmqpService) UnBindUser(userId int) error {
	amqpChannel, err := createAmqpChannel(makeShardQueueName())
	if err != nil {
		return err
	}

	return amqpChannel.QueueUnbind(
		makeShardQueueName(),
		makeRoutingKey(userId),
		environment.Config.AmqpExchangePostsFeed,
		nil,
	)
}

func makeShardQueueName() string {
	return environment.Config.AmqpQueuePostsFeed + "-" + environment.Config.ShardKey
}

func makeRoutingKey(userId int) string {
	return fmt.Sprintf("feed.cache.%d", userId)
}

func createAmqpChannel(queueName string) (*amqp.Channel, error) {
	connection, err := amqp.Dial(environment.Config.AmqpDsn)
	if err != nil {
		return nil, err
	}

	channel, err := connection.Channel()
	if err != nil {
		return nil, err
	}

	_, err = channel.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}

	go func() {
		for {
			reason, ok := <-channel.NotifyClose(make(chan *amqp.Error))
			if !ok || channel.IsClosed() {
				logrus.Errorf("channel closed by the reason: %s", reason)

				_ = channel.Close() // close again, ensure closed flag set when connection closed

				os.Exit(1)
			}
		}
	}()

	return channel, nil
}
