package service

import (
	"fmt"
	"github.com/go-redis/redis"
	"social-network-backend/pkg/environment"
	"time"
)

const (
	feedCachePrefix = "feed-"
)

type RedisService struct {
}

func NewRedisService() *RedisService {
	return &RedisService{}
}

func (s *RedisService) FetchFeed(userId int) (string, error) {
	redisClient, err := createRedisClient()
	if err != nil {
		return "", err
	}

	return redisClient.Get(makeCacheKey(userId)).Result()
}

func (s *RedisService) SaveFeed(userId int, value string) error {
	redisClient, err := createRedisClient()
	if err != nil {
		return err
	}

	_, err = redisClient.Set(makeCacheKey(userId), value, 0*time.Second).Result()

	return err
}

func makeCacheKey(userId int) string {
	return fmt.Sprintf("%s%d", feedCachePrefix, userId)
}

func createRedisClient() (*redis.Client, error) {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", environment.Config.RedisHost, environment.Config.RedisPort),
		Password: environment.Config.RedisPass,
		DB:       environment.Config.RedisDbName,
	})

	_, err := redisClient.Ping().Result()
	if err != nil {
		return nil, err
	}

	return redisClient, nil
}
