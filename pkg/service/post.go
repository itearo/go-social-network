package service

import (
	"social-network-backend/pkg/model"
	"social-network-backend/pkg/repository/mysql"
	"social-network-backend/pkg/repository/tarantool"
)

type PostService struct {
	mysqlRepo     *mysql.PostMysqlRepository
	tarantoolRepo *tarantool.PostTarantoolRepository
}

func NewPostService(mysqlRepo *mysql.PostMysqlRepository, tarantoolRepo *tarantool.PostTarantoolRepository) *PostService {
	return &PostService{mysqlRepo: mysqlRepo, tarantoolRepo: tarantoolRepo}
}

func (s *PostService) CreatePost(postInput model.PostInput) error {
	return s.mysqlRepo.CreatePost(postInput)
}

func (s *PostService) GetFeed(userId int) ([]model.PostFeedItem, error) {
	//return s.mysqlRepo.GetFeed(userId)
	return s.tarantoolRepo.GetFeed(userId)
}
