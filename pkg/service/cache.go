package service

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"social-network-backend/pkg/environment"
	"social-network-backend/pkg/model"
)

type CacheService struct {
	userService  *UserService
	postService  *PostService
	amqpService  *AmqpService
	redisService *RedisService
}

func NewCacheService(
	userService *UserService,
	postService *PostService,
	amqpService *AmqpService,
	redisService *RedisService,
) *CacheService {
	return &CacheService{
		userService:  userService,
		postService:  postService,
		amqpService:  amqpService,
		redisService: redisService,
	}
}

func (s *CacheService) ListenForFeedCacheEvents() error {
	events, err := s.amqpService.ConsumeFeedCacheEvents()
	if err != nil {
		return err
	}

	var rebuildFeedCacheEvent RebuildFeedCacheEvent

	for event := range events {
		logrus.Infof("received a message: %s", event.Body)

		err = json.Unmarshal(event.Body, &rebuildFeedCacheEvent)
		if err != nil {
			logrus.Error("can not deserialize event: %s", err)
		}

		err = s.RebuildFeedCache(rebuildFeedCacheEvent)
		if err != nil {
			logrus.Error("can not rebuild cache: %s", err)
		}
	}

	return nil
}

func (s *CacheService) RebuildFeedCache(rebuildFeedCacheEvent RebuildFeedCacheEvent) error {
	postFeedItems, err := s.postService.GetFeed(rebuildFeedCacheEvent.UserId)
	if err != nil {
		return err
	}

	return s.SaveFeedCache(rebuildFeedCacheEvent.UserId, postFeedItems)
}

func (s *CacheService) SaveFeedCache(userId int, postFeedItems []model.PostFeedItem) error {
	serializedPostFeed, err := json.Marshal(postFeedItems)
	if err != nil {
		return err
	}

	err = s.redisService.SaveFeed(userId, string(serializedPostFeed))
	if err != nil {
		return err
	}

	logrus.Infof("saved feed cache for user #%d", userId)

	return nil
}

func (s *CacheService) FetchFeedCache(userId int) ([]model.PostFeedItem, error) {
	serializedPostFeed, err := s.redisService.FetchFeed(userId)
	if err != nil {
		return nil, err
	}

	var postFeedItems []model.PostFeedItem

	err = json.Unmarshal([]byte(serializedPostFeed), &postFeedItems)
	if err != nil {
		return nil, err
	}

	logrus.Infof("fetched feed cache for user #%d", userId)

	return postFeedItems, nil
}

func (s *CacheService) SendFeedRebuildEvents(userId int) error {
	user, err := s.userService.GetById(userId)
	if err != nil {
		return err
	}

	if len(user.Friends) > environment.Config.FeedCacheThreshold {
		logrus.Warnf(
			"caching skipped because user have too much friends (greater than %d)",
			environment.Config.FeedCacheThreshold,
		)

		return nil
	}

	for idx := range user.Friends {
		logrus.Warn(user.Friends[idx].Id)
		err = s.amqpService.PublishFeedCacheEvent(RebuildFeedCacheEvent{UserId: user.Friends[idx].Id})
		if err != nil {
			return err
		}
	}

	return nil
}

type RebuildFeedCacheEvent struct {
	UserId int `json:"userId"`
}
