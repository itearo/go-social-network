package model

type User struct {
	Id         int    `json:"-"          db:"id"`
	Email      string `json:"email"      binding:"required"`
	Password   string `json:"password"   binding:"required"`
	Name       string `json:"name"       binding:"required"`
	FamilyName string `json:"familyName" binding:"required" db:"family_name"`
	Age        string `json:"age"        binding:"required"`
	Sex        string `json:"sex"        binding:"required"`
	Interests  string `json:"interests"  binding:"required"`
	City       string `json:"city"       binding:"required"`
}

type UserList struct {
	Id         int        `json:"id"         db:"id"`
	Name       string     `json:"name"`
	FamilyName string     `json:"familyName" db:"family_name"`
	Age        string     `json:"age"`
	Sex        string     `json:"sex"`
	Interests  string     `json:"interests"`
	City       string     `json:"city"`
	Friends    []UserList `json:"friends"`
}

type UserPublic struct {
	Id         int    `json:"id"         db:"id"`
	Name       string `json:"name"`
	FamilyName string `json:"familyName" db:"family_name"`
	Age        string `json:"age"`
	Sex        string `json:"sex"`
	Interests  string `json:"interests"`
	City       string `json:"city"`
}
