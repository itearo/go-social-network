package model

type PostInput struct {
	Message  string `json:"message" binding:"required"`
	AuthorId int
}

type PostFeedItem struct {
	Id      int        `json:"id"      db:"id"`
	Message string     `json:"message" db:"message"`
	Author  UserPublic `json:"author"`
}
