package tarantool

import (
	"fmt"

	gotarantool "github.com/tarantool/go-tarantool"
)

const (
	PostsTable           = "POSTS"
	UsersTable           = "USERS"
	UsersFriendshipTable = "USERS_FRIENDSHIP"
)

func NewTarantoolConnection(host string, port string) (*gotarantool.Connection, error) {
	opts := gotarantool.Opts{User: "guest"}
	conn, err := gotarantool.Connect(fmt.Sprintf("%s:%s", host, port), opts)
	if err != nil {
		return nil, err
	}

	return conn, nil
}
