package tarantool

import (
	"fmt"
	gotarantool "github.com/tarantool/go-tarantool"
	"social-network-backend/pkg/model"
)

const (
	postFeedRecordLimit = "1000"
)

type PostTarantoolRepository struct {
	connection *gotarantool.Connection
}

func NewPostTarantoolRepository(connection *gotarantool.Connection) *PostTarantoolRepository {
	return &PostTarantoolRepository{connection: connection}
}

func (r *PostTarantoolRepository) CreatePost(postInput model.PostInput) error {
	return fmt.Errorf("not implemented")
}

func (r *PostTarantoolRepository) GetFeed(userId int) ([]model.PostFeedItem, error) {
	posts := make([]model.PostFeedItem, 0)

	query := "SELECT posts.id as post_id, posts.message, users.id as user_id, name, family_name, age, sex, interests, city " +
		"FROM " + PostsTable + " AS posts " +
		"JOIN " + UsersTable + " AS users ON posts.author_id = users.id " +
		"JOIN " + UsersFriendshipTable + " AS friends ON friends.user_id = posts.author_id WHERE friends.friend_id = ? " +
		"LIMIT " + postFeedRecordLimit
	resp, err := r.connection.Execute(query, []interface{}{userId})
	if err != nil {
		return posts, err
	}

	for key := range resp.Tuples() {
		data := resp.Tuples()[key]
		posts = append(posts, model.PostFeedItem{
			Id:      int(data[0].(uint64)),
			Message: data[1].(string),
			Author: model.UserPublic{
				Id:         int(data[2].(uint64)),
				Name:       data[3].(string),
				FamilyName: data[4].(string),
				Age:        data[5].(string),
				Sex:        data[6].(string),
				Interests:  data[7].(string),
				City:       data[8].(string),
			},
		})
	}

	return posts, nil
}
