package repository

import (
	"github.com/jmoiron/sqlx"
	gotarantool "github.com/tarantool/go-tarantool"
	"social-network-backend/pkg/repository/mysql"
	"social-network-backend/pkg/repository/tarantool"
)

type Repository struct {
	AuthRepository          *mysql.AuthMysql
	UserRepository          *mysql.UserMysql
	ChatRepository          *mysql.ChatMysqlRepository
	PostRepository          *mysql.PostMysqlRepository
	PostTarantoolRepository *tarantool.PostTarantoolRepository
}

func NewRepository(masterDb *sqlx.DB, slaveDb *sqlx.DB, tarantoolConn *gotarantool.Connection) *Repository {
	return &Repository{
		AuthRepository:          mysql.NewAuthMysql(masterDb),
		UserRepository:          mysql.NewUserRepository(masterDb, slaveDb),
		ChatRepository:          mysql.NewChatMysqlRepository(masterDb),
		PostRepository:          mysql.NewPostRepository(masterDb),
		PostTarantoolRepository: tarantool.NewPostTarantoolRepository(tarantoolConn),
	}
}
