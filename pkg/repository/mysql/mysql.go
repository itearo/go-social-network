package mysql

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"strings"
)

const (
	UsersTable             = "users"
	UsersFriendshipTable   = "users_friendship"
	PostsTable             = "posts"
	ChatMessageUnreadTable = "chat_message_unread"

	QueryHardRecordLimit = 10000
)

type Config struct {
	Username string
	Password string
	Host     string
	Port     string
	DBName   string
}

func NewMysqlDB(cfg Config) (*sqlx.DB, error) {
	db, err := sqlx.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true&charset=utf8",
		cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.DBName))

	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func buildSelectQuery(table string, alias string, fields []string) string {
	var aliasedFields []string

	for idx := range fields {
		aliasedFields = append(aliasedFields, table+"."+fields[idx]+" AS \""+alias+"."+fields[idx]+"\"")
	}

	return strings.Join(aliasedFields, ", ")
}
