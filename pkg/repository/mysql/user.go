package mysql

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"social-network-backend/pkg/model"
)

type UserMysql struct {
	masterDb *sqlx.DB
	slaveDb  *sqlx.DB
}

func NewUserRepository(masterDb *sqlx.DB, slaveDb *sqlx.DB) *UserMysql {
	return &UserMysql{masterDb: masterDb, slaveDb: slaveDb}
}

func (r *UserMysql) GetAll() ([]model.UserList, error) {
	var users []model.UserList

	query := fmt.Sprintf("SELECT id, name, family_name, age, sex, interests, city FROM %s LIMIT %d", UsersTable, QueryHardRecordLimit)
	err := r.masterDb.Select(&users, query)

	for idx := range users {
		var friends []model.UserList

		query := fmt.Sprintf("SELECT t1.id, name, family_name, age, sex, interests, city FROM %s t1 JOIN %s t2 ON t1.id = t2.friend_id WHERE user_id = ?", UsersTable, UsersFriendshipTable)
		err := r.masterDb.Select(&friends, query, users[idx].Id)

		if friends == nil {
			friends = make([]model.UserList, 0)
		}

		users[idx].Friends = friends

		if err != nil {
			return users, err
		}
	}

	return users, err
}

func (r *UserMysql) AddFriend(userId, friendId int) error {
	query := fmt.Sprintf("INSERT INTO %s (user_id, friend_id) VALUES (?, ?)", UsersFriendshipTable)
	_, err := r.masterDb.Exec(query, userId, friendId)

	return err
}

func (r *UserMysql) GetById(id int) (model.UserList, error) {
	var user model.UserList

	query := fmt.Sprintf("SELECT id, name, family_name, age, sex, interests, city FROM %s WHERE id = ?", UsersTable)
	row := r.masterDb.QueryRow(query, id)
	err := row.Scan(&user.Id, &user.Name, &user.FamilyName, &user.Age, &user.Sex, &user.Interests, &user.City)

	var friends []model.UserList

	query = fmt.Sprintf("SELECT t1.id, name, family_name, age, sex, interests, city FROM %s t1 JOIN %s t2 ON t1.id = t2.friend_id WHERE user_id = ?", UsersTable, UsersFriendshipTable)
	err = r.masterDb.Select(&friends, query, id)

	if friends == nil {
		friends = make([]model.UserList, 0)
	}

	user.Friends = friends

	if err != nil {
		return user, err
	}

	return user, err
}

func (r *UserMysql) GetByQuery(query string) ([]model.UserList, error) {
	var users []model.UserList

	query = query + "%"

	q := fmt.Sprintf("SELECT id, name, family_name, age, sex, interests, city FROM %s WHERE name LIKE '%s' AND family_name LIKE '%s' ORDER BY id", UsersTable, query, query)
	err := r.slaveDb.Select(&users, q)

	return users, err
}
