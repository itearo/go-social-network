package mysql

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"social-network-backend/pkg/model"
)

const (
	postFeedRecordLimit = "1000"
)

type PostMysqlRepository struct {
	db *sqlx.DB
}

func NewPostRepository(db *sqlx.DB) *PostMysqlRepository {
	return &PostMysqlRepository{db: db}
}

func (r *PostMysqlRepository) CreatePost(postInput model.PostInput) error {
	query := fmt.Sprintf("INSERT INTO %s (message, author_id) values (?, ?)", PostsTable)

	_, err := r.db.Exec(query, postInput.Message, postInput.AuthorId)

	return err
}

func (r *PostMysqlRepository) GetFeed(userId int) ([]model.PostFeedItem, error) {
	posts := make([]model.PostFeedItem, 0)
	authorFields := buildSelectQuery("user", "author", []string{"id", "name", "family_name", "age", "sex", "interests", "city"})
	query := "SELECT post.id, post.message, " + authorFields + " FROM " + PostsTable + " AS post " +
		"JOIN " + UsersTable + " AS user ON post.author_id = user.id " +
		"JOIN " + UsersFriendshipTable + " AS friends ON friends.user_id = post.author_id WHERE friends.friend_id = ? " +
		"LIMIT " + postFeedRecordLimit
	err := r.db.Select(&posts, query, userId)

	return posts, err
}
