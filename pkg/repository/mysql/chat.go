package mysql

import (
	"fmt"
	"github.com/jmoiron/sqlx"
)

type ChatMysqlRepository struct {
	db *sqlx.DB
}

func NewChatMysqlRepository(db *sqlx.DB) *ChatMysqlRepository {
	return &ChatMysqlRepository{db: db}
}

func (r *ChatMysqlRepository) AddOneUnreadMessage(senderId int, addressId int) error {
	query := fmt.Sprintf("INSERT INTO %s (sender_id, addressee_id, counter) VALUES (?, ?, 1) ON DUPLICATE KEY UPDATE counter = counter + 1", ChatMessageUnreadTable)
	_, err := r.db.Exec(query, senderId, addressId)

	return err
}

func (r *ChatMysqlRepository) ClearUnreadMessages(senderId int, addressId int) error {
	query := fmt.Sprintf("INSERT INTO %s (sender_id, addressee_id, counter) VALUES (?, ?, 0) ON DUPLICATE KEY UPDATE counter = 0", ChatMessageUnreadTable)
	_, err := r.db.Exec(query, senderId, addressId)

	return err
}

func (r *ChatMysqlRepository) GetUnreadMessagesCount(addresseeId int) (count int, err error) {
	_ = r.db.QueryRow("SELECT sum(counter) FROM "+ChatMessageUnreadTable+" WHERE addressee_id = ?", addresseeId).Scan(&count)

	return count, err
}
