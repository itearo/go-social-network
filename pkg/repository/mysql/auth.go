package mysql

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"social-network-backend/pkg/model"
)

type AuthMysql struct {
	db *sqlx.DB
}

func NewAuthMysql(db *sqlx.DB) *AuthMysql {
	return &AuthMysql{db: db}
}

func (r *AuthMysql) CreateUser(user model.User) error {
	query := fmt.Sprintf("INSERT INTO %s (email, password_hash, name, family_name, age, sex, interests, city) values (?, ?, ?, ?, ?, ?, ?, ?)", UsersTable)

	_, err := r.db.Exec(query, user.Email, user.Password, user.Name, user.FamilyName, user.Age, user.Sex, user.Interests, user.City)
	if err != nil {
		return err
	}

	return nil
}

func (r *AuthMysql) GetUser(username string) (model.User, error) {
	var user model.User
	query := fmt.Sprintf("SELECT id FROM %s WHERE email = ?", UsersTable)
	err := r.db.Get(&user, query, username)

	return user, err
}
