package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"social-network-backend/pkg/model"
)

func (h *Handler) createPost(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	var postInput model.PostInput

	if err := c.BindJSON(&postInput); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	postInput.AuthorId = userId

	err = h.services.PostService.CreatePost(postInput)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	err = h.services.CacheService.SendFeedRebuildEvents(userId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"success": true,
	})
}

type getPostFeedResponse struct {
	Data []model.PostFeedItem `json:"data"`
}

func (h *Handler) getFeed(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	posts, err := h.services.CacheService.FetchFeedCache(userId)

	if err != nil || posts == nil {
		logrus.Infof("feed cache for user #%d is empty, will fetch data from database", userId)

		posts, err = h.services.PostService.GetFeed(userId)
		if err != nil {
			newErrorResponse(c, http.StatusInternalServerError, err.Error())
			return
		}

		err = h.services.CacheService.SaveFeedCache(userId, posts)
		if err != nil {
			logrus.Warnf("can not save feed cache: %s", err)
		}

	}

	c.JSON(http.StatusOK, getPostFeedResponse{
		Data: posts,
	})
}
