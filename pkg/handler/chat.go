package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"social-network-backend/pkg/model"
	"strconv"
)

func (h *Handler) saveNewMessage(c *gin.Context) {
	_, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	friendId, err := strconv.Atoi(c.Param("friendId"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid friendId param")
		return
	}

	var messageInput model.ChatMessageInput

	if err := c.BindJSON(&messageInput); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	authToken := c.GetHeader(authHeaderName)

	correlationId, err := getCorrelationId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	err = h.services.ChatService.SaveNewMessage(authToken, friendId, messageInput.Message, correlationId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"success": true,
	})
}

func (h *Handler) getChatMessages(c *gin.Context) {
	if c.Param("friendId") == "unread" {
		h.getUnreadMessagesCount(c)

		return
	}

	_, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	friendId, err := strconv.Atoi(c.Param("friendId"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid friendId param")
		return
	}

	authToken := c.GetHeader(authHeaderName)

	correlationId, err := getCorrelationId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	chatMessages, err := h.services.ChatService.GetChatMessages(authToken, friendId, correlationId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	type getChatMessagesResponse struct {
		Data []model.ChatMessage `json:"data"`
	}

	c.JSON(http.StatusOK, getChatMessagesResponse{
		Data: chatMessages,
	})
}

func (h *Handler) getChats(c *gin.Context) {
	_, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	authToken := c.GetHeader(authHeaderName)

	correlationId, err := getCorrelationId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	chats, err := h.services.ChatService.GetChats(authToken, correlationId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	type getChatResponse struct {
		Data []model.Chat `json:"data"`
	}

	c.JSON(http.StatusOK, getChatResponse{
		Data: chats,
	})
}

func (h *Handler) getUnreadMessagesCount(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	count, err := h.services.ChatService.GetUnreadMessagesCount(userId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	type getUnreadMessagesCountResponse struct {
		Data int `json:"data"`
	}

	c.JSON(http.StatusOK, getUnreadMessagesCountResponse{
		Data: count,
	})
}
