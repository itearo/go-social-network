package handler

import (
	"github.com/gin-gonic/gin"
	"social-network-backend/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	auth := router.Group("/auth")
	{
		auth.POST("/sign-up", h.signUp)
		auth.POST("/sign-in", h.signIn)
	}

	api := router.Group("/api", h.apiMiddleware)
	{
		user := api.Group("/user")
		{
			user.GET("/list", h.getAll)
			user.POST("/add-friend", h.addFriend)
			user.GET("/profile/:id", h.getById)
			user.GET("/search/:query", h.getByQuery)
		}

		post := api.Group("/post")
		{
			post.POST("/", h.createPost)
			post.GET("/feed", h.getFeed)
		}

		chat := api.Group("/chat")
		{
			chat.GET("/", h.getChats)
			chat.POST("/:friendId", h.saveNewMessage)
			chat.GET("/:friendId", h.getChatMessages)
		}
	}

	return router
}
