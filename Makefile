include .env
export

.DEFAULT_GOAL := help

CURRENT_PATH := $(dir $(abspath $(MAKEFILE_LIST)))

GOOSE_ARGS := -dir='db/migrations' -table='_db_version' mysql '${MASTER_USER}:${MASTER_PASS}@tcp(${MASTER_HOST}:${MASTER_PORT})/${DB_NAME}?parseTime=true'
GOOSE_SHARD_ARGS := -dir='db/migrations' -table='_db_version' mysql '${MASTER_SHARD_USER}:${MASTER_SHARD_PASS}@tcp(${MASTER_SHARD_HOST}:${MASTER_SHARD_PORT})/${DB_NAME}?parseTime=true'

.PHONY: help
help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z1-9_\-\/\.]+:.*?## / {printf "  \033[32m%-24s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

run: ## Run application
	@docker-compose up -d --build

db/migrate: ## Migrate DB up
	@docker-compose exec app goose ${GOOSE_ARGS} up

db/rollback: ## Migrate DB down
	@docker-compose exec app goose ${GOOSE_ARGS} down

db/shard/migrate: ## Migrate DB up
	@docker-compose exec app goose ${GOOSE_SHARD_ARGS} up

db/shard/rollback: ## Migrate DB down
	@docker-compose exec app goose ${GOOSE_SHARD_ARGS} down

tests/run: ## Run unit tests
	@docker-compose exec app go test -v ./...
