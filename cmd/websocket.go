package main

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"html/template"
	"net/http"
	"os"
	"social-network-backend/pkg/environment"
	"social-network-backend/pkg/repository"
	"social-network-backend/pkg/repository/mysql"
	"social-network-backend/pkg/repository/tarantool"
	"social-network-backend/pkg/service"
	"strconv"
)

var services *service.Service

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type ConnectUser struct {
	Websocket *websocket.Conn
	UserId    int
}

func IndexHandler(w http.ResponseWriter, _ *http.Request) {
	tmpl, _ := template.ParseFiles("templates/websocket_test_page.html")
	if err := tmpl.Execute(w, nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

var clients = make(map[int]*ConnectUser)

func WebsocketHandler(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.Atoi(r.URL.Query().Get("userId"))
	if err != nil {
		logrus.Error("could not parse userId query parameter", err)
		return
	}

	ws, _ := upgrader.Upgrade(w, r, nil)

	defer func() {
		if err = ws.Close(); err != nil {
			logrus.Error("Websocket could not be closed", err)
		}
	}()

	socketClient := &ConnectUser{
		Websocket: ws,
		UserId:    userId,
	}
	logrus.Info("Client connected:", socketClient.UserId)
	clients[socketClient.UserId] = socketClient

	err = services.AmqpService.BindUser(userId)
	if err != nil {
		logrus.Error("could not parse bind user to queue", err)
		return
	}

	for {
		_, _, err = ws.ReadMessage()
		if err != nil {
			logrus.Info("Ws disconnect waiting: ", err)
			delete(clients, userId)

			err = services.AmqpService.UnBindUser(userId)
			if err != nil {
				logrus.Error("could not parse bind user to queue", err)
				return
			}

			return
		}
	}
}

type rebuildFeedCacheEvent struct {
	UserId int `json:"userId"`
}

func listenForFeedReloadEvents() error {
	events, err := services.AmqpService.ConsumeFeedReloadEvents()
	if err != nil {
		return err
	}

	var rebuildEvent rebuildFeedCacheEvent

	for event := range events {
		logrus.Infof("received a message: %s", event.Body)

		err = json.Unmarshal(event.Body, &rebuildEvent)
		if err != nil {
			logrus.Error("can not deserialize event: %s", err)
		}

		if client, ok := clients[rebuildEvent.UserId]; ok {
			if err = client.Websocket.WriteMessage(websocket.TextMessage, []byte("news feed updated, reload data from api")); err != nil {
				logrus.Error("Cloud not send Message to ", rebuildEvent.UserId, err)
			}

			logrus.Info("Message sent to ", rebuildEvent.UserId)
		}
	}

	return nil
}

func main() {
	environment.Boot()

	logrus.Warn(environment.Config)

	masterDb, err := mysql.NewMysqlDB(mysql.Config{
		Host:     environment.Config.MasterHost,
		Port:     environment.Config.MasterPort,
		Username: environment.Config.MasterUser,
		Password: environment.Config.MasterPass,
		DBName:   environment.Config.DbName,
	})

	if err != nil {
		logrus.Fatalf("failed to initialize master db: %s", err)
	}

	tarantoolConnection, err := tarantool.NewTarantoolConnection(
		environment.Config.TarantoolHost,
		environment.Config.TarantoolPort,
	)

	if err != nil {
		logrus.Fatalf("failed to initialize tarantool connection: %s", err.Error())
	}

	repos := repository.NewRepository(masterDb, masterDb, tarantoolConnection)
	services = service.NewServices(repos, environment.Config)

	go func() {
		if err = listenForFeedReloadEvents(); err != nil {
			logrus.Fatalf("error occured while running amqp consumer: %s", err.Error())

			os.Exit(1)
		}
	}()

	http.HandleFunc("/", IndexHandler)
	http.HandleFunc("/ws", WebsocketHandler)
	logrus.Fatal(http.ListenAndServe("0.0.0.0:3000", nil))
}
