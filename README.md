# Dummy Social Network API written in GoLang

### Installation <a name="installation"></a>

1. Run `make` with no arguments to view all available shortcuts
2. Run `make run` to run all the application services
2. Run `make db/migrate` to migrate DB schema
2. Run `make db/shard/migrate` to migrate shard DB schema
2. API available at http://localhost:8000
