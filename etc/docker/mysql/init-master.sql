CREATE USER IF NOT EXISTS 'soc_net'@'%' IDENTIFIED BY 'soc_net';
GRANT ALL PRIVILEGES ON soc_net.* TO 'soc_net'@'%';

CREATE USER IF NOT EXISTS 'replication_user'@'%' IDENTIFIED BY 'replication_password';
GRANT REPLICATION SLAVE ON *.* TO 'replication_user'@'%';

FLUSH PRIVILEGES;
