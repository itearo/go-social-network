CHANGE MASTER TO
    MASTER_HOST = 'db',
    MASTER_USER = 'replication_user',
    MASTER_PASSWORD = 'replication_password',
    MASTER_AUTO_POSITION = 1
;
START SLAVE;
