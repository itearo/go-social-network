
local function bootstrap()

    if not box.space.mysql_bin_log then
        box.schema.user.grant('guest','read,write,execute,create,drop', 'universe', nil, {if_not_exists = true})

        mysql_bin_log = box.schema.space.create('mysql_bin_log')
        mysql_bin_log:create_index('primary', { type = 'tree', parts = { 1, 'integer'}, if_not_exists = true})

        posts = box.schema.space.create('POSTS', {
            format = {
                [1] = {["name"] = "ID", ["type"] = "integer"},
                [2] = {["name"] = "MESSAGE", ["type"] = "string"},
                [3] = {["name"] = "AUTHOR_ID", ["type"] = "integer"}
            }
        })
        posts:create_index('primary', { type = 'tree', parts = { 1, 'integer'}, if_not_exists = true})
        posts:create_index('author_id', { type = 'tree', parts = { 1, 'integer'}, if_not_exists = true})

        users = box.schema.space.create('USERS', {
            format = {
                [1] = {["name"] = "ID", ["type"] = "integer"},
                [2] = {["name"] = "EMAIL", ["type"] = "string"},
                [3] = {["name"] = "PASSWORD_HASH", ["type"] = "string"},
                [4] = {["name"] = "NAME", ["type"] = "string"},
                [5] = {["name"] = "FAMILY_NAME", ["type"] = "string"},
                [6] = {["name"] = "AGE", ["type"] = "string"},
                [7] = {["name"] = "SEX", ["type"] = "string"},
                [8] = {["name"] = "INTERESTS", ["type"] = "string"},
                [9] = {["name"] = "CITY", ["type"] = "string"}
            }
        })
        users:create_index('primary', { type = 'tree', parts = { 1, 'integer'}, if_not_exists = true})
        users:create_index('author_id', { type = 'tree', parts = { 1, 'integer'}, if_not_exists = true})

        users_friendship = box.schema.space.create('USERS_FRIENDSHIP', {
            format = {
                [1] = {["name"] = "ID", ["type"] = "integer"},
                [2] = {["name"] = "USER_ID", ["type"] = "integer"},
                [3] = {["name"] = "FRIEND_ID", ["type"] = "integer"}
            }
        })
        users_friendship:create_index('primary', { type = 'tree', parts = { 1, 'integer'}, if_not_exists = true})
    end

end

bootstrap()
